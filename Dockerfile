FROM python:3.7-slim

RUN pip install --no-cache notebook

ARG NB_USER
ARG NB_UID
ENV USER ${NB_USER}
ENV HOME /home/${NB_USER}

RUN adduser --disabled-password \
    --gecos "Default user" \
    --uid ${NB_UID} \
    ${NB_USER}
WORKDIR ${HOME}

COPY apt_packages.txt /tmp/apt_packages.txt

RUN apt-get update && apt-get --no-install-recommends install --yes $(cat /tmp/apt_packages.txt) && \
    apt-get autoremove --yes && apt-get clean all && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ADD ./requirements.txt /tmp/requirements.txt

RUN pip3 install -r /tmp/requirements.txt

# Install EnergyPlus and various packages

ENV ENERGYPLUS_VERSION 9.0.1
ENV ENERGYPLUS_TAG v9.0.1
ENV ENERGYPLUS_SHA bb7ca4f0da

ENV ENERGYPLUS_DOWNLOAD_BASE_URL https://github.com/NREL/EnergyPlus/releases/download/$ENERGYPLUS_TAG
ENV ENERGYPLUS_DOWNLOAD_FILENAME EnergyPlus-$ENERGYPLUS_VERSION-$ENERGYPLUS_SHA-Linux-x86_64.sh
ENV ENERGYPLUS_DOWNLOAD_URL $ENERGYPLUS_DOWNLOAD_BASE_URL/$ENERGYPLUS_DOWNLOAD_FILENAME

RUN pip3 install six
RUN pip3 install dataclasses
RUN pip3 install besos
RUN pip3 install papermill
RUN pip3 install nbinteract
RUN pip3 install ipysheet
RUN pip3 install nbgitpuller

RUN curl -SLO $ENERGYPLUS_DOWNLOAD_URL
RUN chmod +x $ENERGYPLUS_DOWNLOAD_FILENAME && echo "y\r" | ./$ENERGYPLUS_DOWNLOAD_FILENAME
RUN rm $ENERGYPLUS_DOWNLOAD_FILENAME

# Install GLPK

RUN curl -SLO http://ftp.gnu.org/gnu/glpk/glpk-4.65.tar.gz
RUN tar -xzf glpk-4.65.tar.gz
RUN cd glpk-4.65 && \
    ./configure && \
    make && \
    make check && \
    make install && \
    cd .. && \
    rm -r glpk-4.65 && \
    rm glpk-4.65.tar.gz

ENV LD_LIBRARY_PATH /lib:/usr/lib:/usr/local/lib

# Install Bonmin

RUN curl -SLO https://ampl.com/dl/open/bonmin/bonmin-linux64.zip
RUN unzip bonmin-linux64.zip -d /usr/local/bin && \
    rm bonmin-linux64.zip

# Add notebooks

COPY Examples $HOME
