import os
import pandas as pd

def run_all_models(bp_model, dc_model, eb_model, data_directory, preprocess, results_directory,
                 T_out_col, T_in_col, heat_col, heat_col_2, dt_col): 
    not_found, i, i_prev = 0, 0, 0
    bp_results, dc_results, eb_results = [],[],[]
    for idd in os.listdir(data_directory):
        if idd.endswith(".csv"):
            if (i % 100 == 0) and (i != i_prev):
                i_prev += 1
                print('starting', i)
            fname = os.path.join(data_directory, idd)
            try:
                df = preprocess(fname)
            except FileNotFoundError:
                continue

            # skip if columns are null
            if (df[T_out_col].isna().all() or df[T_in_col].isna().all()
                or df[heat_col].isna().all()):
                    continue

            # skip if there are uxHeat values
            df[heat_col_2] = df.auxHeat2.fillna(0.0)
            if not (df[heat_col_2] == 0.0).all():
                continue

            try:
                dc_result = dc_model(df, fname)
            except ValueError as e:
                # skip because there is no data
                continue

            eb_result = eb_model(df, fname)    
            bp_result = bp_model(df, fname)
            bp_results.append(bp_result)

            # save building csvs
            dc_df = pd.DataFrame(dc_result)
            eb_df = pd.DataFrame(eb_result)
            dc_pth = os.path.join(results_directory, 'decay_curves', idd)
            eb_pth = os.path.join(results_directory, 'energy_balance', idd)
            dc_df.to_csv(dc_pth)
            eb_df.to_csv(eb_pth)
            i += 1


    new_bp = [x for x in bp_results if x]
    bp_df = pd.DataFrame.from_records(new_bp)
    bp_df.to_csv(os.path.join(results_directory, 'bp.csv'))
    print('Done!')
