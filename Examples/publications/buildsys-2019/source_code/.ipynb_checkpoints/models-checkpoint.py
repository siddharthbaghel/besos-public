from time import time
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
import traceback

import source_code.balance_point as bp_methods
import source_code.decay_curves as dc_methods
import source_code.energy_balance as eb_methods
from source_code.select_data_utils import select_dc_intervals, select_eb_intervals, limit_time_ranges

class Model():
    def __init__(self,T_in_col='Thermostat_Temperature', T_out_col='T_out',
                 heat_col='auxHeat1', months=(11,2), hours=(20,5), timestep=5, **kwargs):
        self.months = months
        self.hours = hours
        self.timestep = timestep

        self.T_in_col = T_in_col
        self.T_out_col = T_out_col
        self.heat_col = heat_col
        self.cols = (self.T_in_col, self.T_out_col, self.heat_col)

    def __call__(self, df, iid):
        results = []
        intervals = self.select_intervals(df)
        for i in intervals:
            df_selected = df[i[0]:i[-1]]
            r = self.fit_model(i, df_selected)
            result = self.result_to_dict(i, r, df_selected)
            results.append(result)
        return results

    def select_intervals(self, df): raise Exception('not implemented')
    def fit_model(self, i, df): raise Exception('not implemented')
    def result_to_dict(self, r, iid): raise Exception('not implemented')

class BalancePoint(Model):
    def __init__(self, **kwargs):
        Model.__init__(self, **kwargs)
        self.reject_outliers = kwargs.get('reject_outliers')

    def __call__(self, df, iid):
        try:
            limited_df = self.select_intervals(df)
            result = self.fit_model(limited_df)
            return self.result_to_dict(result, iid)
        except ValueError as e:
            pass

    def select_intervals(self, df):
        return limit_time_ranges(self.months, self.hours, df)

    def fit_model(self, df):
        return bp_methods.find_slope(df, self.reject_outliers, self.cols)

    def result_to_dict(self, r, iid):
        return bp_methods.line_to_dict(r, iid)

class DecayCurves(Model):
    def __init__(self, T_in_derivative_threshold=0., T_in_out_diff=5.,
                 proportion_heating=0.1, minimum_intervals=2., initial_guess=[100., 10.], **kwargs):
        Model.__init__(self, **kwargs)
        self.initial_guess=initial_guess

        # TODO - remove params that are no longer relevant
        self.params = (
            0., 0, T_in_derivative_threshold, T_in_out_diff, proportion_heating, 0 ,0,
            minimum_intervals, self.timestep
        )

    def __call__(self, df, iid):
        return Model.__call__(self, df, iid)

    def select_intervals(self, df):
        r = select_dc_intervals(self.months, self.hours, self.params, df, self.cols)
        return r

    def fit_model(self,i,df):
        return dc_methods.fit_decay_curve(df[self.T_in_col], df[self.T_out_col].mean(), self.initial_guess)

    def result_to_dict(self, i, r, df):
        T_in_mean = df[self.T_in_col].mean()
        T_out_mean = df[self.T_out_col].mean()
        return dc_methods.result_to_dict(i[0], i[-1], T_in_mean, T_out_mean, r)

class EnergyBalance(Model):
    # TODO - could include test_model later.. leave out for now unless we plan to rewrite buildsys paper

    def __init__(self, interval_amnt=30, duration=3, heating_runtime_upper_bound=0.8,
                 heating_runtime_lower_bound=0.05, indoor_variance_threshold=0.2,
                 inital_guess=[0.01, 100], print_times=False, **kwargs):
        Model.__init__(self, **kwargs)
        self.inital_guess = inital_guess
        self.params = (
            interval_amnt, duration, heating_runtime_upper_bound, heating_runtime_lower_bound,
            indoor_variance_threshold, inital_guess
        )
        self.print_times = print_times

    def __call__(self, df, iid, use_power=False):
        results= []
        if use_power: raise Exception('not implemented')

        # TODO - for now only using Ralph's method, could add back over fitting later
        for _ in range(10):
            try:
                intervals = self.to_tuples(self.select_intervals(df))
                if len(intervals) != 0:
                    r = self.fit_model(df, intervals)
                    result = self.result_to_dict(r, intervals)
                    results.append(result)
            except ValueError as e:
                pass

        return results


    @staticmethod
    def to_tuples(intervals):
        new_ints = []
        for i in intervals:
            new_ints.append((i[0],i[-1]))
        return new_ints

    def select_intervals(self, df):
        s = time()
        r = select_eb_intervals(self.months, self.hours, df, self.params)
        if self.print_times: print('select interval time', time() - s)
        return r

    def fit_model(self,df,i):
        s = time()
        r = eb_methods.fit_model(df,self.inital_guess,i)
        if self.print_times: print('fit model time', time() - s)
        return r

    def result_to_dict(self, r, i):
        s = time()
        r = eb_methods.solution_to_dict(r, i)
        if self.print_times: print('to dict time', time() - s)
        return r