import pandas as pd
import numpy as np
from scipy import stats
from source_code.select_data_utils import limit_time_ranges

def resample_data(df, cols):
    
    t_in, t_out, heat_energy = cols
    
    resampled = df.resample('D').mean()
    df_xy = resampled[[t_out, heat_energy]]
    df_xy_nonzero = df_xy.loc[df_xy[heat_energy] != 0]
    
    df_final = df_xy_nonzero.dropna()     
    return df_final

def reject_outliers_balance_point(data, cols):
    
    t_in, t_out, heat_energy = cols
    
    u = np.mean(data[t_out])
    s = np.std(data[t_out])
    data_filtered = data[(data[t_out]>(u-1*s)) & (data[t_out]<(u+1*s))]
    return data_filtered

def find_slope(df, reject_outliers, cols):
    
    t_in, t_out, heat_energy = cols
    df_final = resample_data(df, cols)
    if reject_outliers:
        df_final = reject_outliers_balance_point(df_final, cols)
        
    x = df_final[t_out]
    y = df_final[heat_energy]
    line = stats.linregress(x, y)

    return line


def analyze_building_balance_point(months, hours, filename, building_df, reject_outliers, cols):
    
    
    limited_building_df = limit_time_ranges(months, hours, building_df)

    line = find_slope(limited_building_df, reject_outliers, cols)
    result = line_to_dict(line, filename)
    return result


def line_to_dict(line, filename):
    """Take relevant parts of scipy stats line object"""
    slope = line.slope
    intercept = line.intercept
    r_value = line.rvalue
    p_value = line.pvalue
    stderr = line.stderr
    
    return {
            "filename" : filename, "slope" : slope, "intercept" : intercept,
            "r_value" : r_value, "p_value" : p_value, "stderr" : stderr
    }